/*
 * Copyright (C) 2021  Emanuele Sorce
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * parabola is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtSystemInfo 5.0
import QtQuick.Window 2.2

MainView {
	id: root
	objectName: 'mainView'
	applicationName: 'parabola.emanuelesorce'
	automaticOrientation: true

	width: units.gu(45)
	height: units.gu(75)

	ScreenSaver {
		id: screen_saver
		screenSaverEnabled: false
	}
	
	Settings {
		id: settings
		property int sample_size: 16
		property int sample_rate: 16000
		property int channel_count: 2
	}
	
	PageStack {
		id: pageStack
		Component.onCompleted: push(mainPage)
		
		Page {
			id: mainPage
			anchors.fill: parent

			Component.onCompleted: {
				API.setSampleSize(settings.sample_size);
				API.setSampleRate(settings.sample_rate);
				API.setChannelCount(settings.channel_count);
				API.init();
				API.start();
			}
			
			header: PageHeader {
				id: header
				title: i18n.tr('Parabola')
				
				trailingActionBar {
					actions: [
						Action {
							iconName: "settings"
							text: "settings"
							onTriggered: pageStack.push("qrc:///Settings.qml")
						},
						Action {
							iconName: "info"
							text: "about"
							onTriggered: pageStack.push("qrc:///About.qml")
						}
					]
				}
			}

			Column {
				spacing: units.gu(6)
				anchors {
					margins: units.gu(2)
					top: header.bottom
					left: parent.left
					right: parent.right
					bottom: parent.bottom
				}

				Item {
					width: 1
					height: units.dp(100)
				}
				
				Label {
					horizontalAlignment: Text.AlignHCenter
					width: parent.width
					text: i18n.tr('Microphone input is getting forwarded to audio output') + '<br><br>' + i18n.tr("Screen won't turn off when this app is running")
				}
				
				Label {
					horizontalAlignment: Text.AlignHCenter
					width: parent.width
					color: "red"
					visible: API.lagging
					text: "<b>" + i18n.tr("AUDIO FRAMESKIP!") + "</b>"
				}
			}
		}
	}
}
