/* Copyright 2020 Emanuele Sorce
 * 
 * This file is part of Parabola and is distributed under the terms of
 * the GPL. See the file COPYING for full details.
 */

import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick 2.9
import QtQuick.Layouts 1.3

Page {
	
	header: PageHeader {
		id: header
		title: i18n.tr('Settings')
		
		leadingActionBar {
			actions: [
				Action {
					iconName: "back"
					text: "back"
					onTriggered: pageStack.pop()
				}
			]
		}
	}
	
	Flickable {
		id: flickable
		anchors.fill: parent
		contentHeight:  layout.height + units.dp(80)
		contentWidth: parent.width
		
		Column {
			id: layout

			spacing: units.gu(4)
			anchors.top: parent.top
			anchors.left: parent.left
			anchors.right: parent.right
			anchors.leftMargin: units.gu(5)
			anchors.rightMargin: units.gu(5)
			
			Item {
				height: units.dp(30)
				width: units.dp(5)
			}
			
			Label {
				text: "Requires restart"
				width: parent.width
			}
			
			Label {
				text: "Actual values will depends on hardware and software compability"
				width: parent.width
			}
			
			OptionSelector {
				id: ss
				text: i18n.tr("Preferred sample size:")
				expanded: true
				model: [
					i18n.tr("8 bits - low"),
					i18n.tr("16 bits - high"),
					i18n.tr("24 bits - professional"),
					i18n.tr("32 bits - extreme")
				]
				onSelectedIndexChanged: {
					if(selectedIndex == 0) settings.sample_size = 8;
					if(selectedIndex == 1) settings.sample_size = 16;
					if(selectedIndex == 2) settings.sample_size = 24;
					if(selectedIndex == 3) settings.sample_size = 32;
				}
			}
			
			OptionSelector {
				id: sr
				text: i18n.tr("Preferred sample rate:")
				expanded: true
				model: [
					i18n.tr("8000 Hz - normal"),
					i18n.tr("16000 Hz - high"),
					i18n.tr("32000 Hz - very high"),
					i18n.tr("44100 Hz - professional")
				]
				
				onSelectedIndexChanged: {
					if(selectedIndex == 0) settings.sample_rate = 8000;
					if(selectedIndex == 1) settings.sample_rate = 16000;
					if(selectedIndex == 2) settings.sample_rate = 32000;
					if(selectedIndex == 3) settings.sample_rate = 44100;
				}
			}
			
			OptionSelector {
				id: cc
				text: i18n.tr("Preferred number of channels:")
				expanded: true
				model: [
					i18n.tr("Mono"),
					i18n.tr("Stereo")
				]
				onSelectedIndexChanged: {
					if(selectedIndex == 0) settings.channel_count = 1;
					if(selectedIndex == 1) settings.channel_count = 2;
				}
			}
			
			Component.onCompleted: {
				console.log("Settings sample size: " + settings.sample_size)
				console.log("Settings sample rate: " + settings.sample_rate)
				console.log("Settings channel count: " + settings.channel_count)
				
				if(settings.sample_size == 8) ss.selectedIndex = 0
				if(settings.sample_size == 16) ss.selectedIndex = 1
				if(settings.sample_size == 24) ss.selectedIndex = 2
				if(settings.sample_size == 32) ss.selectedIndex = 3
				
				if(settings.sample_rate == 8000) sr.selectedIndex = 0
				if(settings.sample_rate == 16000) sr.selectedIndex = 1
				if(settings.sample_rate == 32000) sr.selectedIndex = 2
				if(settings.sample_rate == 44100) sr.selectedIndex = 3
				
				if(settings.channel_count == 1) cc.selectedIndex = 0
				if(settings.channel_count == 2) cc.selectedIndex = 1
			}
		}
	}
}
